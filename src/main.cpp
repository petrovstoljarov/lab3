#include <iostream>
#include <limits>
#include "primeNumbers.hpp"

int main(int argc, char *argv[])
{
    int number;
	std::string input;
	
	if (argc == 2)
	{
		input = argv[1];
		for (char c : input)
        	{
            if (!std::isdigit(c))
            {
				std::cout << "Необходимо передавать целое неотрицательное число.";
                return -1;
            }
       	}
		number = std::stoi(input);
	}
	else
    {
    	while (true)
    	{
        	std::cout << "Введите целое неотрицательное число: ";
        	std::cin >> input;
        	bool isValid = true;
        	for (char c : input)
        	{
        	    if (!std::isdigit(c))
         	   {
          	      isValid = false;
          	      break;
           	   }
        	}
        	if (isValid)
        	{
            	number = std::stoi(input);
            	break;
        	}
        	else
        	{
            std::cout << "Ошибка ввода: введите целое неотрицательное число." << std::endl;
        	}
        }
    }

    std::cout << (isPrime(number) ? "Число простое" : "Число составное") << std::endl;
    return 0;
}