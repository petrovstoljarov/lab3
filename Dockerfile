# Используем базовый образ с Docker
FROM ubuntu:latest

# Устанавливаем необходимые пакеты
RUN apt-get update && apt-get install -y dpkg

# Копируем .deb пакет в образ
COPY ./build/debian/*.deb /tmp/

# Устанавливаем .deb пакет
RUN dpkg -i /tmp/*.deb 

CMD ["bash"]


