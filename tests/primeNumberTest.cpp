#include <gtest/gtest.h>
#include "../src/primeNumbers.hpp"

TEST(IsNumberPrime, PrimeNumber) {
	int enteredNumber = 7;
	ASSERT_TRUE(isPrime(enteredNumber));
}

TEST(IsNumberPrime, NotPrimeNumber) {
	int enteredNumber = 6;
	ASSERT_FALSE(isPrime(enteredNumber));
}
